import React, { lazy, Suspense } from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'

import { LoadingMessage } from 'components/loading-message'
import { NavBar } from 'components/nav-bar'

const Home = lazy(() => import('./home'))
const About = lazy(() => import('./about'))
const NoMatch = lazy(() => import('./no-match'))
const TopicList = lazy(() => import('./topic-list'))
const TopicDetail = lazy(() => import('components/topic-detail'))

const Routes = () => (
	<>
		<NavBar />

		<Suspense fallback={<LoadingMessage />}>
			<Switch>
				<Route exact path="/Home" component={Home} />
				<Route exact path="/">
					<Redirect to="/Home" />
				</Route>
				<Route exact path="/About" component={About} />
				<Route exact path="/Topics" component={TopicList} />
				<Route path="/Topics/:topicId" component={TopicDetail} />
				<Route component={NoMatch} />
			</Switch>
		</Suspense>
	</>
)

export default Routes
